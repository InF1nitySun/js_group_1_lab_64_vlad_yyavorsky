import React, {Component} from 'react';
import './NoteShortDesc.css';
import axios from 'axios';


class NoteShortDesc extends Component {


    // componentDidMount(){
    //
    //     const id = this.props.match.params.id;
    //     const postURL = `/posts/${id}.json`;
    //
    //     axios.get(postURL).then((response) => {
    //         console.log(response.data);
    //     })
    //
    // }
    readMore = (id) => {
        this.setState({loading: true});
        const postURL = `/items/${id}.json`;
        // console.log(postURL);

        axios.get(postURL).then((response) => {
            console.log(response.data);

            // this.setState({posts: response.data});
            // this.props.history.replace(postURL);
            // this.setState({loading: false});
        });
    };


    render() {
        // console.log(this.props.posts);
        return (
            <div className="NoteShortDesc">
                <ul className='Quotes'>
                    {Object.keys(this.props.posts).map(id => {
                        return <li className='Quote' key={id}>
                            <span>{this.props.posts[id].date}</span>
                            <h3>{this.props.posts[id].title}</h3>
                            <button onClick={() => this.readMore(id)}>Read more >></button>
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}

export default NoteShortDesc;