import React, {Component, Fragment} from 'react';
import {Route, Switch} from 'react-router-dom';
import './App.css';
import NotFound from "../../Page/Error/NotFound/NotFound";
import Navigation from "../../Page/Header/Navigation/Navigation";
import HomePage from "../../HomePage/HomePage";
import NoteAdd from "../../PostAdd/NoteAdd";
import About from "../../About/About";
import Contact from "../../Contact/Contact";




class App extends Component {

    render() {
        return (
            <Fragment>
                <Navigation/>
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/posts" component={HomePage}/>
                    <Route path="/add"  component={NoteAdd}/>
                    <Route path="/about"  component={About}/>
                    <Route path="/contact"  component={Contact}/>
                    <Route render={() => <NotFound style={{textAlign: 'center'}} component={NotFound}/>}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
