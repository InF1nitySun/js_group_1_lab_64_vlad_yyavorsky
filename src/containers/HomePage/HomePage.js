import React, {Component} from 'react';
import './HomePage.css';
import NoteShortDesc from "../Notes/NoteShortDesc/NoteShortDesc";
// import NoteAdd from "../PostAdd/NoteAdd";
import axios from 'axios';

class HomePage extends Component {

    state = {
        posts: {},
        loading: false
    };

    componentDidMount() {
        this.getPosts();
    }

    getPosts = event => {
        this.setState({loading: true});

        axios.get('/items.json').then(response => {
            this.setState({posts: response.data});
            console.log(response.data);

            // this.props.history.replace('/');
        });
    };




    render() {

        return (
            <div className="HomePage Container">
                <div>
                    <h2>Лабораторная работа 64</h2>
                    <NoteShortDesc posts={this.state.posts}/>
                </div>
            </div>
        )
    }
};

export default HomePage;